<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\UnitRumah;

class ProductController extends Controller
{

    function CreateUnit(Request $request){
        
        DB::beginTransaction();

        try{
            $this->validate($request, [
                'kavling' => 'required',
                'blok' => 'required', 
                'no_rumah' => 'required',
                'harga_rumah' => 'required', 
                'luas_tanah' => 'required',
                'luas_bangunan' => 'required'
            ]);

            $kavling = $request->input('kavling');
            $blok = $request->input('blok');
            $no_rumah = $request->input('no_rumah');
            $harga_rumah = $request->input('harga_rumah');
            $luas_tanah = $request->input('luas_tanah');
            $luas_bangunan = $request->input('luas_bangunan');

            $product = new UnitRumah;
            $product->kavling = $kavling;
            $product->blok = $blok;
            $product->no_rumah = $no_rumah;
            $product->harga_rumah = $harga_rumah;
            $product->luas_tanah = $luas_tanah;
            $product->luas_bangunan = $luas_bangunan;
            $product->save();

            DB::commit();
            return response()->json(["message" => "Success !!!"], 200);
        }
        catch(\Exception $e){
                    
            DB::rollback();
            return response()->json(["message" => $e->getMessage()], 500);
        }

    }

    function DeleteUnit(Request $request){
        $completedList = $request->deleted;
        if(!empty($completedList))
        {
            foreach ($completedList as $val) {
                DB::delete('delete from unit where id = ?', [$val]);
            }
        }
        return redirect("/");
    }

}
